module Api
  module V1
    class AttendancesController < ApplicationController
      def update_attendance
        student = Student.find_by_legajo(params[:message][:legajo])
        inscriptions = Inscription.where("student_id = ?", student.id)
        fecha = DateTime.parse(params[:message][:datetime])
        dia = I18n.l(fecha, format: '%A')
        calendar_day = CalendarDay.where("fecha = ?", fecha.beginning_of_day..fecha.end_of_day).first
        hora_llegada = Time.parse(fecha.strftime("%H:%M"))
        inscriptionSelected = nil
        inscriptions.each do |inscription|
          inicio = Time.parse(inscription.schedule.timetable.hour_init.strftime("%H:%M"))
          fin = Time.parse(inscription.schedule.timetable.hour_end.strftime("%H:%M"))
          if hora_llegada.between?(inicio,fin) && inscription.schedule.day == dia
            inscriptionSelected = inscription
          end
        end
        attendance = Attendance.where("inscription_id = ? AND calendar_day_id = ?",inscriptionSelected.id, calendar_day.id).first
        if attendance.status == false
          attendance.update_attribute(:status, true)
        end
        render json: attendance
      end
    end
  end
end
