module Api
  module V1
    class ClassroomsController < ApplicationController
      def index
        @classrooms = Classroom.all
        render json: @classrooms
      end

      def show
      end

      def create
        classroom = Classroom.new(classroom_params)
        if classroom.save
          render json: classroom
        else
          render json: {status: 'ERROR', message: 'Classroom no saved', data: classroom.errors}, status: :unprocessable_entity
        end
      end

      private
      def classroom_params
        params.permit(:nro, :description)
      end
    end
  end
end