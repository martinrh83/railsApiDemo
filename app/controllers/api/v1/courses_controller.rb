module Api
  module V1
    class CoursesController < ApplicationController
      def index
        @courses = Course.all
        render json: @courses
      end

      def show
        course = Course.find(params[:id])
        render json: course
      end

      def create
        grade = Grade.find(course_params[:grade_id])
        course = Course.new(course_params.merge(name: grade.level.to_s + course_params[:name]))
        if course.save
          render json: course
        else
          render json: {error: true, data: course.errors.full_messages}
        end
      end

      def destroy
        course = Course.find(params[:id])
        grade = Grade.find(course.grade_id)
        grade.courses.find(course.id).destroy
      end

      private
      def course_params
        params.require(:course).permit(:name, :grade_id, :turno)
      end
    end
  end
end