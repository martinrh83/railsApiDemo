module Api
  module V1
    class GradesController < ApplicationController
      def index
        @grades = Grade.all
        render json: @grades
      end

      def show
        @grades = Grade.find(params[:id])
        render json: @grades
      end

      def create
        grades = Grade.new(grade_params)
        if grades.save
          render json: grades
        else
          render json: {error: true, data: grades.errors.full_messages}
        end
      end

      def destroy
        grade = Grade.find(params[:id])
        school_year = SchoolYear.find(grade.school_year_id)
        school_year.grades.find(grade.id).destroy
      end

      private
      def grade_params
        params.require(:grade).permit(:level, :school_year_id)
      end
    end
  end
end