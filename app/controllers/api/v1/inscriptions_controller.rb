module Api
  module V1
    class InscriptionsController < ApplicationController
      def index
        inscriptions = Student.find(params[:student_id]).inscriptions
        render json: inscriptions
      end

      def create
        student_id = params[:student_id]
        params[:schedules].each do |schedule|
          schedule_id = schedule["id"]
          inscription = Inscription.new
          inscription.schedule_id = schedule_id
          inscription.student_id = student_id
          if inscription.save
            arrayDates = Schedule.find(schedule_id).calendar_days
            insc = Inscription.where("schedule_id = ? AND student_id = ? ",schedule_id,student_id).first
            arrayDates.each do |date|
              attendance = Attendance.new
              attendance.inscription_id = insc.id
              attendance.calendar_day_id = date.id
              attendance.save
            end
          else
            puts "ya estas incripto"
          end
        end
      end

      private
      def inscription_params
        params.require(:inscription).permit(:student_id, :schedule_id)
      end
    end
  end
end