module Api
  module V1
    class SchedulesController < ApplicationController
      def index
        @schedules = Course.find(params[:course_id]).schedules
        render json: @schedules
      end

      def getSchedule
        schedule = Schedule.find(params[:id])
        render json: schedule
      end

      def create
        arraySchedules = []
        params[:selectedHs].each do |hora|
          timetable_id = hora["id"]
          schedule = Schedule.new(schedules_params)
          schedule.timetable_id = timetable_id
          if schedule.save
            arraySchedules << schedule
          else
            old_schedule = Schedule.where("timetable_id = ? AND course_id = ? AND subject_id =? AND day=?",timetable_id ,params[:horario][:course_id],params[:horario][:subject_id], params[:horario][:day])
            arraySchedules << old_schedule.first
          end
        end
        #["2018-10-02T00:00:00-03:00", "2018-10-09T00:00:00-03:00"]
        if params[:selectedDaysArr]
          params[:selectedDaysArr].each do |f|
            fecha = DateTime.parse(f)
            day = nil
            if CalendarDay.any?
              if CalendarDay.where("fecha = ?", fecha ).exists?
                day = CalendarDay.where("fecha = ?", fecha ).first
              else
                day = CalendarDay.create(fecha: fecha)
              end
              arraySchedules.each do |schedule|
                schedule.calendar_days << day unless schedule.calendar_days.include?(day)
              end
            else
              day = CalendarDay.create(fecha: fecha)
              arraySchedules.each do |schedule|
                schedule.calendar_days << day
              end
            end
          end
        end
        #date1 = DateTime.parse("2018-10-02T00:00:00-03:00")
        #date2 = DateTime.parse("2018-10-09T00:00:00-03:00")
        #schedule = Schedule.new(schedules_params)
        #if schedule.save
        #  render json: schedule
        #else
        #  render json: {status: 'ERROR', message: 'Schedules not saved', data: schedule.errors}, status: :unprocessable_entity
        #end
      end

      def schedulesFiltered
        schedules = Schedule.where("course_id = ? AND subject_id = ?", params[:course_id], params[:subject_id])
        render json: schedules

      end
      private
      def schedules_params
        params.require(:horario).permit(:day, :course_id, :subject_id, :classroom_id, :timetable_id)
      end
    end
  end
end
