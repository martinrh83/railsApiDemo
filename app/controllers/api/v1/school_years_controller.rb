module Api
  module V1
    class SchoolYearsController < ApplicationController
      def index
        @school_year = SchoolYear.all
        render json: @school_year
      end

      def show
        @school_year = SchoolYear.find(params[:id])
        render json: @school_year
      end

      def create
        school_year = SchoolYear.new(school_year_params)
        if school_year.save
          render json: school_year
        else
          render json: {error: true, data: school_year.errors.full_messages}
        end
      end

      def destroy
        school_year = SchoolYear.find(params[:id])
        school_year.destroy
      end

      def current_school_year
        year = Date.current.year
        current_grades = SchoolYear.where("year = ?", year.to_s ).first.grades
        render json: current_grades
      end

      private
      def school_year_params
        params.require(:school_year).permit(:year)
      end
    end
  end
end