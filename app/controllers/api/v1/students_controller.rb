
module Api
  module V1
    class StudentsController < ApplicationController
      def index
        @students = Student.all
        render json: @students
      end

      def show
        @students = Student.find(params[:id])
        render json: @students
      end

      def create
        params_completed = student_params.merge(password: params[:password])
        student = Student.new(params_completed)
        if student.save
          #response = Unirest.post "http://jorgearias1:-d350la01@192.168.0.19:8000/mr/ajax/save_alumno", 
          #    headers:{ "Accept" => "application/json" }, 
          #    parameters:{ :name => params[:student][:name], :last_name => params[:student][:last_name], :legajo => params[:student][:legajo], :email => params[:student][:email]}
          render json: student
        else
          render json: {status: 'ERROR', message: 'Student not saved', data: student.errors}, status: :unprocessable_entity
        end
      end

      def login
        student = Student.where("legajo = ?", params[:legajo])
        if student != []
          render json: {error: false, message: 'Existe el usuario', token: student.first.token, student_id: student.first.id}
        else
          render json: {error: true, message: 'Datos ingresados no válidos'}
        end
      end

      private
      def student_params
        params.require(:student).permit(:name, :last_name, :legajo, :email, :password, :password_confirmation)
      end
    end
  end
end