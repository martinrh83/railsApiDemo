module Api
  module V1
    class SubjectsController < ApplicationController
      def index
        #@subjects = Subject.includes(:grade).order("grades.level desc")
        @subjects = Subject.all
        render json: @subjects
      end

      def show
        @subjects = Subject.find(params[:id])
        render json: @subjects
      end

      def create
        subject = Subject.new(subject_params)
        if subject.save
          render json: subject
        else
          render json: {error: true, message: 'Subject no saved', data: subject.errors.full_messages}
        end
      end

      def destroy
        subject = Subject.find(params[:id])
        grade = Grade.find(subject.grade_id)
        grade.subjects.find(subject.id).destroy
      end

      private
      def subject_params
        params.require(:subject).permit(:name, :code, :grade_id, :period)
      end
    end
  end
end