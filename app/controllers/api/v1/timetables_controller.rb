module Api
  module V1
    class TimetablesController < ApplicationController
      def index
        timetables = Timetable.all
        render json: timetables
      end

      def create
        timetable = Timetable.new(timetable_params)
        if timetable.save
          render json: timetable
        else
          render json: {error: true, data: timetable.errors.full_messages}
        end
      end

      def destroy
        timetable = Timetable.find(params[:id])
        timetable.destroy
      end

      private
      def timetable_params
        params.require(:timetable).permit(:hour_init, :hour_end)
      end
    end
  end
end