class CalendarDay < ApplicationRecord
  has_and_belongs_to_many :schedules, -> { distinct }
  has_many :attendances, dependent: :destroy 
  validates :fecha, uniqueness: true
end
