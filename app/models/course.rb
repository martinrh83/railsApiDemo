class Course < ApplicationRecord
  belongs_to :grade
  has_many :schedules,  dependent: :destroy
  validates :turno, :name, presence: true
  validates :name, uniqueness: {scope: [:grade_id]}
end
