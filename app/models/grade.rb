class Grade < ApplicationRecord
  belongs_to :school_year
  has_many :subjects,  dependent: :destroy
  has_many :courses,  dependent: :destroy

  validates :level, presence: true
  validates :level, uniqueness: {scope: [:school_year_id]}
  validates :level, numericality: { less_than_or_equal_to: 5,  greater_than: 0, only_integer: true }
end
