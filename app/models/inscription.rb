class Inscription < ApplicationRecord
  has_many :attendances, dependent: :destroy 
  belongs_to :student
  belongs_to :schedule
  validates :student_id, uniqueness: {scope: [:schedule_id]}
end
