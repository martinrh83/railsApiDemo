class Schedule < ApplicationRecord
  belongs_to :course
  belongs_to :subject
  belongs_to :classroom
  belongs_to :timetable
  has_many :inscriptions, -> { distinct }, dependent: :destroy 
  has_and_belongs_to_many :calendar_days, -> { distinct }, dependent: :destroy 
  validates :course_id, uniqueness: {scope: [:subject_id, :classroom_id, :timetable_id, :day]}
end
