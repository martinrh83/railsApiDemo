class SchoolYear < ApplicationRecord
  has_many :grades,  dependent: :destroy
  validates :year, presence: true
  validates :year, uniqueness: true
  validates :year, length: { is: 4 }
  before_destroy :destroy_grades

  private

    def destroy_grades
      self.grades.destroy_all   
    end
end
