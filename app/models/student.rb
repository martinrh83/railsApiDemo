class Student < ApplicationRecord
  has_many :inscriptions, -> { distinct }
  has_secure_password
  has_secure_token
end
