class Subject < ApplicationRecord
  belongs_to :grade
  has_many :schedules, dependent: :destroy

  validates :name, :code, :period, presence: true
  validates :name, uniqueness: {scope: [:grade_id]}
end
