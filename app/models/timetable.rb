class Timetable < ApplicationRecord
  has_many :schedules, dependent: :destroy

  validates :hour_init, :hour_end, presence: true
  validates :hour_init, :hour_end, uniqueness: true
end
