class User < ApplicationRecord
  has_many :posts
  validates :name, :last_name, :email, presence: true

end
