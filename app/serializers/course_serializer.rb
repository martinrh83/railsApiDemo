class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :turno
  belongs_to :grade
end