class GradeSerializer < ActiveModel::Serializer
  attributes :id, :level, :courses, :subjects
  belongs_to :school_year
end
