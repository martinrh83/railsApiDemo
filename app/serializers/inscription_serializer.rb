class InscriptionSerializer < ActiveModel::Serializer
  attributes :id, :attendances
  belongs_to :schedule
  belongs_to :student
  def attendances
    customized_attendances = []

    object.attendances.each do |attendance|
      custom_attendance = attendance.attributes
      custom_attendance[:id] = attendance.id
      custom_attendance[:status] = attendance.status
      custom_attendance[:calendar_day_id] = attendance.calendar_day.fecha
      customized_attendances.push(custom_attendance)
    end

    return customized_attendances
  end
end