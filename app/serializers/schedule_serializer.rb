class ScheduleSerializer < ActiveModel::Serializer
  attributes :id, :day, :calendar_days
  belongs_to :course
  belongs_to :subject
  belongs_to :timetable
  belongs_to :classroom
end