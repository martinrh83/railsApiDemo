class SchoolYearSerializer < ActiveModel::Serializer
  attributes :id, :year, :grades

  def grades
    customized_grades = []

    object.grades.each do |grade|
      # Assign object attributes (returns a hash)
      # ===========================================================
      custom_grade = grade.attributes


      # Custom nested and side-loaded attributes
      # ===========================================================
      # belongs_to
      custom_grade[:id] = grade.id # get only :id and :name for the project
      custom_grade[:level] = grade.level
      # has_many w/only specified attributes
      custom_grade[:courses] = grade.courses.collect{|course| course.slice(:id, :name)}

      # ===========================================================
      customized_grades.push(custom_grade)
    end

    return customized_grades
  end
end
