class StudentSerializer < ActiveModel::Serializer
  attributes :id, :name, :last_name, :legajo, :email
end