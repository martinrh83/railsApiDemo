class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :code, :period
  belongs_to :grade
end