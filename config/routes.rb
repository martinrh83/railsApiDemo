Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace 'api' do
      namespace 'v1' do
        resources :posts
        resources :users
        resources :school_years
        resources :grades
        resources :subjects
        resources :classrooms
        resources :timetables
        resources :courses do
          resources :schedules
        end
        resources :students do
          resources :inscriptions
        end
        post 'students/login', to: 'students#login'
        get 'current_year', to: 'school_years#current_school_year'
        get 'course/:course_id/subject/:subject_id/schedules', to: 'schedules#schedulesFiltered'
        get 'schedule/:id', to: 'schedules#getSchedule'
        patch 'attendances/update', to: 'attendances#update_attendance'
      end
  end
end
