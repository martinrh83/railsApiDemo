class CreateGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :grades do |t|
      t.integer :level
      t.references :school_year, foreign_key: true

      t.timestamps
    end
  end
end
