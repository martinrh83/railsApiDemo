class AddTokenToStudents < ActiveRecord::Migration[5.1]
  def change
    add_column :students, :token, :string
  end
end
