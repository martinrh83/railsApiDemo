class CreateTimetables < ActiveRecord::Migration[5.1]
  def change
    create_table :timetables do |t|
      t.string :hour_init
      t.string :hour_end

      t.timestamps
    end
  end
end
