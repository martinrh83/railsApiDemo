class CreateCalendarDays < ActiveRecord::Migration[5.1]
  def change
    create_table :calendar_days do |t|
      t.datetime :fecha

      t.timestamps
    end
  end
end
