class CreateJoinTableCaledarDaySchedule < ActiveRecord::Migration[5.1]
  def change
    create_join_table :calendar_days, :schedules do |t|
       t.index [:calendar_day_id, :schedule_id], unique: true, name: :idx_calendar_day_schedules_calendar_day_schedule
       t.index [:schedule_id, :calendar_day_id], unique: true, name: :idx_calendar_day_schedules_schedule_calendar_day
    end
  end
end
