class AddTimetableToSchedules < ActiveRecord::Migration[5.1]
  def change
    add_reference :schedules, :timetable, foreign_key: true
  end
end
