class ChangeColumnsTypeOnTimetables < ActiveRecord::Migration[5.1]
  def up
    change_column :timetables, :hour_init, :time
    change_column :timetables, :hour_end, :time
  end

  def down
    change_column :timetables, :hour_init, :string
    change_column :timetables, :hour_end, :string
  end
end
