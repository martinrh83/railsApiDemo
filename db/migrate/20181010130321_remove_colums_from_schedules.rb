class RemoveColumsFromSchedules < ActiveRecord::Migration[5.1]
  def up
    remove_column :schedules, :hour_init
    remove_column :schedules, :hour_end
  end

  def down
    add_column :schedules, :hour_init, :time
    add_column :schedules, :hour_end, :time
  end
end
