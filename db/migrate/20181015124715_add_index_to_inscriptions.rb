class AddIndexToInscriptions < ActiveRecord::Migration[5.1]
  def change
    add_index :inscriptions, [:student_id, :schedule_id], :unique => true
  end
end
