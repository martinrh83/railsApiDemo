class CreateAttendances < ActiveRecord::Migration[5.1]
  def change
    create_table :attendances do |t|
      t.boolean :status, :default => false
      t.references :inscription, foreign_key: true
      t.references :calendar_day, foreign_key: true

      t.timestamps
    end
  end
end
