# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181028201018) do

  create_table "attendances", force: :cascade do |t|
    t.boolean "status", default: false
    t.integer "inscription_id"
    t.integer "calendar_day_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["calendar_day_id"], name: "index_attendances_on_calendar_day_id"
    t.index ["inscription_id"], name: "index_attendances_on_inscription_id"
  end

  create_table "calendar_days", force: :cascade do |t|
    t.datetime "fecha"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calendar_days_schedules", id: false, force: :cascade do |t|
    t.integer "calendar_day_id", null: false
    t.integer "schedule_id", null: false
    t.index ["calendar_day_id", "schedule_id"], name: "idx_calendar_day_schedules_calendar_day_schedule", unique: true
    t.index ["schedule_id", "calendar_day_id"], name: "idx_calendar_day_schedules_schedule_calendar_day", unique: true
  end

  create_table "classrooms", force: :cascade do |t|
    t.string "nro"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.integer "grade_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "turno"
    t.index ["grade_id"], name: "index_courses_on_grade_id"
  end

  create_table "grades", force: :cascade do |t|
    t.integer "level"
    t.integer "school_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_year_id"], name: "index_grades_on_school_year_id"
  end

  create_table "inscriptions", force: :cascade do |t|
    t.integer "student_id"
    t.integer "schedule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["schedule_id"], name: "index_inscriptions_on_schedule_id"
    t.index ["student_id", "schedule_id"], name: "index_inscriptions_on_student_id_and_schedule_id", unique: true
    t.index ["student_id"], name: "index_inscriptions_on_student_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.string "day"
    t.integer "course_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "classroom_id"
    t.integer "timetable_id"
    t.index ["classroom_id"], name: "index_schedules_on_classroom_id"
    t.index ["course_id"], name: "index_schedules_on_course_id"
    t.index ["subject_id"], name: "index_schedules_on_subject_id"
    t.index ["timetable_id"], name: "index_schedules_on_timetable_id"
  end

  create_table "school_years", force: :cascade do |t|
    t.string "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "legajo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "email"
    t.string "token"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "period"
    t.integer "grade_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["grade_id"], name: "index_subjects_on_grade_id"
  end

  create_table "timetables", force: :cascade do |t|
    t.time "hour_init"
    t.time "hour_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
